# pylint: disable=line-too-long
# pylint: disable=invalid-name

from typing import Tuple
from typing import List
from typing import Union
from decimal import Decimal
from copy import deepcopy
from operator import add, sub, mul
import array

import math

class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """
    def __init__(self, shape: Tuple[int], dtype: type = None, data: Union[Tuple, List] = None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        _check_shape(shape)
        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n_elements = 1
        for s in self.__shape:
            n_elements *= s

        if data is None:
            if dtype == int:
                self.__data = array.array('i', [0]*n_elements)
            elif dtype == float:
                self.__data = array.array('f', [0]*n_elements)
        else:
            if len(data) != n_elements:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    @property
    def data(self):
        return self.__data

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 1 or ind[ax] > self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds [1, {:}]'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data

        ##### Izpis #####
    def __str__(self):
        temp = '\n'
        array_shape: Tuple[int] = self.shape
        dec_point: int = 0

        depth: int = 1 if len(array_shape) < 3 else array_shape[2]
        is_multi = (len(array_shape) == 3)

        if self.dtype == float:
            for f in self.__data:
                cur_point = abs(Decimal(str(f)).as_tuple().exponent)
                if cur_point > dec_point:
                    dec_point = cur_point

        for d in range(depth):
            for r in range(array_shape[0]):
                for c in range(array_shape[1]):
                    if self.dtype == float: 
                        temp += (_prepend_space(self[r+1, c+1, d+1], dec_point) if is_multi else _prepend_space(self[r+1, c+1], dec_point)) + ' '
                    else:
                        temp += (_prepend_space(self[r+1, c+1, d+1]) if is_multi else _prepend_space(self[r+1, c+1])) + ' '
                temp += '\n'
            temp += '\n' if d != depth - 1 else '' 

        return temp

    def to_string(self):
        return self.__str__()

    ##### Sortiranje #####
    def sort(self, direction='row'):

        dim = len(self.shape)

        cols = 1 if dim < 2 else self.shape[1]
        rows = self.shape[0]

        dim: int = len(self.shape)
        ret_list = list()
        s_list = list()

        if direction == 'row':
            ret_list = list(self.__data[r*cols:(r+1)*cols] for r in range(rows))       
            for row in ret_list:
                s_list.append(_merge_sort(row))  
        else:
            ret_list = list(self.__data[c::cols] for c in range(cols)) 
            for col in ret_list:
                s_list.append(_merge_sort(col))

        # https://stackoverflow.com/questions/952914/making-a-flat-list-out-of-list-of-lists-in-python
        # 2d -> 1d list

        ret_list = [i for subl in s_list for i in subl]

        if direction == 'col':
            return BaseArray(shape=self.shape, dtype=self.dtype, data=tuple([i for subl in [ret_list[c::cols] for c in range(cols)] for i in subl]))
        else:
            return BaseArray(shape=self.shape, dtype=self.dtype, data=tuple(ret_list))

    ##### Iskanje elementa #####
    def search(self, val_to_search):
        if val_to_search not in self.__data:
            return tuple()
        else:
            dim = len(self.shape)
            tup_shape = self.shape
            cols = 1 if dim < 2 else tup_shape[1]
            rows = tup_shape[0]
            depth = 1 if dim < 3 else tup_shape[2]

            if dim == 2:
                return tuple((r + 1, c + 1) for r in range(rows) for c in range(cols) if self[r + 1, c + 1] == val_to_search)
            if dim == 3:
                return tuple((r + 1, c + 1, d + 1) for d in range(depth) for r in range(rows) for c in range(cols) if self[r + 1, c + 1, d+1] == val_to_search)

    ##### Osnovne matematicne operacije #####
    #seštevanje, odštevanje, množenje, deljenje, logaritmiranje in potenciranje
    # https://stackoverflow.com/questions/18713321/element-wise-addition-of-2-lists
    def __add__(self, other):

        other_type = type(other)
        typed:type = self.dtype
        main_type = float if (other_type == float or typed == float) else int

        if other_type == BaseArray:
            _check_operation(self, other)

        dim = len(self.shape)
        values = None

        if other_type == BaseArray:
            if dim <= 3:
                values = list(map(add, self, other))
            else:
                raise Exception('Dimensions out of bounds.')
        elif other_type == int or other_type == float:
            if dim <= 3:    
                values = [main_type(x + other) for x in self]
            else:
                raise Exception('Could not perform calculation for this dimension.')
        else:
            raise Exception('Wrong type of other: {}.'.format(type(other)))
        
        return BaseArray(shape=self.shape, dtype=main_type, data=tuple(values))
    
    def __sub__(self, other):

        other_type = type(other)
        typed:type = self.dtype
        main_type = float if (other_type == float or typed == float) else int

        if other_type == BaseArray:
            _check_operation(self, other)

        dim = len(self.shape)
        values = None

        if other_type == BaseArray:
            if dim <= 3:
                values = list(map(sub, self, other))
            else: 
                raise Exception('Dimensions out of bounds.')
        elif other_type == int or other_type == float:
            if dim <= 3:    
                values = [main_type(x - other) for x in self]
            else:
                raise Exception('Could not perform calculation for this dimension.')
        else:
            raise Exception('Wrong type of other: {}.'.format(type(other)))
        
        return BaseArray(shape=self.shape, dtype= main_type, data=tuple(values))
    
    def __mul__(self, other):

        dim = len(self.shape)
        values = None
        other_type:type = type(other)
        typed:type = self.dtype
        main_type = float if (other_type == float or typed == float) else int

        if other_type == float or other_type == int:
            if dim <= 3:
                values = [main_type(x * other) for x in self]
            else:
                raise Exception('Too many dimensions: {}.'.format(len(self.shape)))

            return BaseArray(shape=self.shape, dtype=main_type, data=tuple(values))
        elif other_type == BaseArray:

            if self.shape[1] != other.shape[0] and len(self.shape) == 2 and len(other.shape) == 2:
                raise Exception('Wrong dimensions of 2D matrices.')

            typed = float if (self.dtype == float or other.dtype == float) else int
            if dim == 2:
                l1 = [x for x in [self.__data[r*self.shape[1]:(r+1)*self.shape[1]] for r in range(self.shape[0])]]
                l2 = [y for y in [other.data[c::other.shape[1]] for c in range(other.shape[1])]]
                values = [_multiply_lists(x, y, typed) for x in l1 for y in l2]
            
            if dim >= 3:
                raise Exception('Multiplication is only allowed on 2D matrices.')

            return BaseArray(shape=(self.shape[0], other.shape[1]), dtype=typed, data=tuple(values))
        else:
            raise Exception('Unsopported type for: {}.'.format(typed))


    def __truediv__(self, other):
        typed = type(other)
        if typed == float or typed == int:
            if other == 0:
                raise Exception('Scalar can not be 0, division 0 exception...')
            return self.__mul__(1/other)
        elif typed == BaseArray:
            raise Exception('Only scalar divisor allowed.')
        else:
            raise Exception('Unsopported type for: {}.'.format(typed))


    def __pow__(self, other):
        typed = type(other)
        if typed == int:
            org = deepcopy(self)
            temp = deepcopy(org)
            i = 1
            while i < other:
                temp *= org
                i = i + 1
            return BaseArray(shape=temp.shape, dtype=temp.dtype, data=temp.data)
        else:
            raise Exception('The power operation can only be done with an int exponent.')


    @staticmethod
    def exponent(base, exponent):
        b_type = type(base)
        ex_type = type(exponent)
        if b_type == BaseArray and (ex_type == int or ex_type == float):
            values = [x**exponent for x in base]
            return BaseArray(shape=base.shape, dtype=base.dtype, data=values)
        elif (b_type == float or b_type == int) and ex_type == BaseArray:
            values = [base**x for x in exponent]
            return BaseArray(shape=exponent.shape, dtype=exponent.dtype, data=values)
        else:
            raise Exception('Unsopported types for exponent operation.')


    @staticmethod
    def log10(matrix):
        return BaseArray(shape=matrix.shape, dtype=matrix.dtype, data=[math.log10(x) for x in matrix])


def _check_shape(shape):
    if not isinstance(shape, (tuple, list)):
        raise Exception(f'shape {shape:} is not a tuple or list')
    for dim in shape:
        if not isinstance(dim, int):
            raise Exception(f'shape {shape:} contains a non integer {dim:}')


def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if isinstance(inds, slice):
        start, stop, step = inds.start, inds.stop, inds.step
        if start is None:
            start = 1
        if step is None:
            step = 1
        if stop is not None:
            stop += 1
        inds_itt = range(0, s+1)[start:stop:step]
    else:  # type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,)+ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += (inds[n]-1)*s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if not indice:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True


def _prepend_space(val, dec_point = 0):
    if dec_point == 0:
        return str(val) if val < 0 else ' '+str(val)
    else:
        return '{:.{}f}'.format(val, dec_point) if val < 0 else ' {:.{}f}'.format(val, dec_point) 


def _merge_sort(arr):
    if len(arr)>1:
        i = j = k = 0
        mid = len(arr)//2
        l_list = arr[:mid]
        r_list = arr[mid:]

        _merge_sort(l_list)
        _merge_sort(r_list)

        while i < len(l_list) and j < len(r_list):
            if l_list[i] > r_list[j]:
                arr[k]=r_list[j]
                j=j+1
            else:
                arr[k]=l_list[i]
                i=i+1
            k=k+1

        __fill_remainder(arr, k, i, l_list)
        __fill_remainder(arr, k, j, r_list)

        return arr

def __fill_remainder(arr, arr_counter, counter, lr_list):
    while counter < len(lr_list):
        arr[arr_counter]=lr_list[counter]
        counter = counter + 1
        arr_counter = arr_counter + 1

def _same_dimensions(m1: BaseArray, m2: BaseArray):
    return m1.shape == m2.shape

def _check_operation(m1: BaseArray, m2: BaseArray):
    dim = len(m1.shape)

    if dim > 3:
        raise Exception('Unsopported dimension of ({}) for addition operation.'.format(dim))

    if not _same_dimensions(m1, m2):
        raise Exception('Dimensions of matrices {} != {} are not equal.'.format(m1.shape, m2.shape))

    if not isinstance(m2, BaseArray):
        raise Exception('The operation must be performed on variables of type: {} only.'.format(type(BaseArray)))

def _multiply_lists(l1:List, l2:List, dtype:type):
    if len(l1) != len(l2):
        raise Exception('Can not multiply lists of different lengths: {} != {}.'.format(len(l1), len(l2)))
    else:
        return dtype(sum(list(map(mul, l1, l2))))
