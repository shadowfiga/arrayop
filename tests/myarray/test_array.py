from unittest import TestCase

from myarray.basearray import BaseArray
import myarray.basearray as ndarray

class TestArray(TestCase):
    def test___init__(self):

        # simple init tests, other things are tester later
        try:
            a = BaseArray((2,))
            a = BaseArray([2, 5])
            a = BaseArray((2, 5), dtype=int)
            a = BaseArray((2, 5), dtype=float)
        except Exception as e:
            self.fail(f'basic constructor test failed, with exception {e:}')

        # test invalid parameters as shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray(5)
        self.assertEqual(('shape 5 is not a tuple or list',),
                         cm.exception.args)

        # test invalid value in shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray((4, 'a'))
        self.assertEqual(('shape (4, \'a\') contains a non integer a',),
                         cm.exception.args)

        # TODO test invalid dtype

        # test valid data param, list, tuple
        a = BaseArray((3, 4), dtype=int, data=list(range(12)))
        a = BaseArray((3, 4), dtype=int, data=tuple(range(12)))

        # test invalid data param
        # invalid data length
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 4), dtype=int, data=list(range(12)))
        self.assertEqual(('shape (2, 4) does not match provided data length 12',),
                         cm.exception.args)

        # inconsistent data type
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 2), dtype=int, data=(4, 'a', (), 2.0))
        self.assertEqual(('provided data does not have a consistent type',),
                         cm.exception.args)

    def test_shape(self):
        a = BaseArray((5,))
        self.assertTupleEqual(a.shape, (5,))
        a = BaseArray((2, 5))
        self.assertTupleEqual(a.shape, (2, 5))
        a = BaseArray((3, 2, 5))
        self.assertEqual(a.shape, (3, 2, 5))

    def test_dtype(self):
        # test if setting a type returns the set type
        a = BaseArray((1,))
        self.assertIsInstance(a[1], float)
        a = BaseArray((1,), int)
        self.assertIsInstance(a[1], int)
        a = BaseArray((1,), float)
        self.assertIsInstance(a[1], float)

    def test_get_set_item(self):
        # test if setting a value returns the value
        a = BaseArray((1,))
        a[1] = 1
        self.assertEqual(1, a[1])
        # test for multiple dimensions
        a = BaseArray((2, 1))
        a[1, 1] = 1
        a[2, 1] = 1
        self.assertEqual(1, a[1, 1])
        self.assertEqual(1, a[2, 1])

        # test with invalid indices
        a = BaseArray((2, 2))
        with self.assertRaises(Exception) as cm:
            a[4, 1] = 0
        self.assertEqual(('indice (4, 1), axis 0 out of bounds [1, 2]',),
                         cm.exception.args)

        # test invalid type of ind
        with self.assertRaises(Exception) as cm:
            a[1, 1.1] = 0
        self.assertEqual(cm.exception.args,
                         ('(1, 1.1) is not a valid indice',))

        with self.assertRaises(Exception) as cm:
            a[1, 'a'] = 0
        self.assertEqual(('(1, \'a\') is not a valid indice',),
                         cm.exception.args)

        # test invalid number of ind
        with self.assertRaises(Exception) as cm:
            a[2] = 0
        self.assertEqual(('indice (2,) is not valid for this myarray',),
                         cm.exception.args)

        # test data intialized via data parameter
        a = BaseArray((2, 3, 4), dtype=int, data=tuple(range(24)))
        self.assertEqual(3, a[1, 1, 4])
        self.assertEqual(13, a[2, 1, 2])

        # TODO enter invalid type

    def test_iter(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in a]
        self.assertListEqual([1, 2, 3, 4, 5, 6], a_vals)

    def test_reversed(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in reversed(a)]
        self.assertListEqual([6, 5, 4, 3, 2, 1], a_vals)

    def test_contains(self):
        a = BaseArray((1,), dtype=float)
        a[1] = 1

        self.assertIn(1., a)
        self.assertIn(1, a)
        self.assertNotIn(0, a)

    def test_shape_to_steps(self):
        shape = (4, 2, 3)
        steps_exp = (6, 3, 1)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = [1]
        steps_exp = (1,)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = []
        steps_exp = ()
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

    def test_multi_to_lin_ind(self):
        # shape = 4, 2,
        # 0 1
        # 2 3
        # 4 5
        # 6 7
        steps = 2, 1
        multi_lin_inds_pairs = (((1, 1), 0),
                                ((2, 2), 3),
                                ((4, 1), 6),
                                ((4, 2), 7))
        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_exp, lin_ind_res)

        # shape = 2, 3, 4
        #
        # 0 1 2 3
        # 4 5 6 7
        #
        #  8  9 10 11
        # 12 13 14 15
        steps = 8, 4, 1
        multi_lin_inds_pairs = (((1, 1, 1), 0),
                                ((2, 2, 1), 12),
                                ((1, 1, 4), 3),
                                ((2, 2, 3), 14))

        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_res, lin_ind_exp)

    def test_is_valid_instance(self):
        self.assertTrue(ndarray._is_valid_indice(2))
        self.assertTrue(ndarray._is_valid_indice((2, 1)))

        self.assertFalse(ndarray._is_valid_indice(()))
        self.assertFalse(ndarray._is_valid_indice([2]))
        self.assertFalse(ndarray._is_valid_indice(2.0))
        self.assertFalse(ndarray._is_valid_indice((2, 3.)))

    def test_multi_ind_iterator(self):
        shape = (30,)
        ind = (slice(10, 20, 3),)
        ind_list_exp = ((10,),
                        (13,),
                        (16,),
                        (19,))
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        shape = (5, 5, 5)
        ind = (2, 2, 2)
        ind_list_exp = ((2, 2, 2),)
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (2, slice(3), 2)
        ind_list_exp = ((2, 1, 2),
                        (2, 2, 2),
                        (2, 3, 2),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (slice(None, None, 2), slice(3), slice(2, 4))
        ind_list_exp = ((1, 1, 2), (1, 1, 3), (1, 1, 4),
                        (1, 2, 2), (1, 2, 3), (1, 2, 4),
                        (1, 3, 2), (1, 3, 3), (1, 3, 4),
                        (3, 1, 2), (3, 1, 3), (3, 1, 4),
                        (3, 2, 2), (3, 2, 3), (3, 2, 4),
                        (3, 3, 2), (3, 3, 3), (3, 3, 4),
                        (5, 1, 2), (5, 1, 3), (5, 1, 4),
                        (5, 2, 2), (5, 2, 3), (5, 2, 4),
                        (5, 3, 2), (5, 3, 3), (5, 3, 4),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

    ##### MY TESTS #####

    def test_to_string(self):
        a = BaseArray(shape=(2,2, 1), dtype=int, data=(-1, 1, 3, -4))
        b = BaseArray(shape=(2,2,3), dtype=int, data=(-1, 1, 3, -4, -1, 1, 3, -4, -1, 1, 3, -4))
        c = BaseArray(shape=(2,2), dtype=float, data=(-1.023, 1.123, -4.12445234, 3.3231))

        self.assertEqual(str(a), '\n-1  1 \n 3 -4 \n')
        self.assertEqual(str(b), '\n-1 -4 \n 3  1 \n\n 1 -1 \n-4  3 \n\n 3  1 \n-1 -4 \n')
        self.assertEqual(c.to_string(), '\n-1.02300000  1.12300000 \n-4.12445234  3.32310000 \n')

    def test_merge_sort(self):
        s_list = [7, 9, 5, 1, 8, 2]
        ndarray._merge_sort(s_list)
        self.assertListEqual(s_list, [1, 2, 5, 7, 8, 9])

    def test_sort_row_col(self):
        a = BaseArray(shape=(3,3), dtype=int, data=(0,1,1,4,0,-2,1,3,0))
        temp = BaseArray(shape=(3,3), dtype=int, data=(0, 1, 1, -2, 0, 4, 0, 1, 3))
        temp2 = BaseArray(shape=(3,3), dtype=int, data=(0, 0, -2, 1, 1, 0, 4, 3, 1))
        b = a.sort('row')
        c = a.sort('col')

        self.assertListEqual(b.data, temp.data)
        self.assertListEqual(c.data, temp2.data)

    def test_search_value(self):
        a = BaseArray(shape=(2, 2, 1), dtype=int, data=(-1, 1, 3, -4))
        b = BaseArray(shape=(2, 2, 3), dtype=int, data=(-1, 1, 3, -4, -1, 1, 3, -4, -1, 1, 3, -4))
        c = BaseArray(shape=(2, 2), dtype=int, data=(-1, 1, 3, -4))
        d = BaseArray(shape=(3, 1), dtype=int, data=(1, 1, 1))

        self.assertTupleEqual(a.search(1)[0], (1, 2, 1))
        self.assertTupleEqual(b.search(1), ((2, 2, 1), (1, 1, 2), (1, 2, 3)))
        self.assertTupleEqual(c.search(1)[0], (1, 2))
        self.assertTupleEqual(d.search(1), ((1, 1), (2, 1), (3, 1)))

    def test_mul_lists(self):
        # for multiplying two lists and adding them together 
        self.assertEqual(ndarray._multiply_lists([-1, -1, -1], [-1, -1, -1], int), 3)

    def test_addition(self):
        a = BaseArray(shape=(2,2), dtype=int, data=(-1, 1, 3, -4))
        b = BaseArray(shape=(2,2), dtype=int, data=(-1, 1, 3, -4))
        self.assertListEqual((a+b).data, [-2, 2, 6, -8])
        self.assertListEqual((a+3).data, [2, 4, 6, -1])

    def test_substraction(self):
        a = BaseArray(shape=(2,2), dtype=int, data=(-1, 1, 3, -4))
        b = BaseArray(shape=(2,2), dtype=int, data=(-12, 12, 32, -42))
        self.assertListEqual((a-b).data, [11, -11, -29, 38] )
        self.assertListEqual((a-4).data, [-5, -3, -1, -8])

    def test_multiplication(self):
        a = BaseArray(shape=(2,2), dtype=int, data=(-1, -1, -1, -1))
        b = BaseArray(shape=(2,2), dtype=int, data=(-12, 12, -1, -1))

        c = BaseArray(shape=(2,3), dtype=int, data=(1, 1, 1, 1, 1, 1))
        d = BaseArray(shape=(3,1), dtype=int, data=(1, 1, 1))
        
        g = a*b
        h = c*d
        e = b*c

        self.assertListEqual(g.data, [13, -11, 13, -11])
        self.assertListEqual(h.data, [3, 3])
        self.assertListEqual(e.data, [0, 0, 0, -2, -2, -2])

    def test_division(self):
        a = BaseArray(shape=(2,2), dtype=int, data=(-1, -1, -1, -1))
        self.assertListEqual((a/-0.5).data, [2, 2, 2, 2])

    def test_pow(self):
        a = (BaseArray(shape=(2,2), dtype=float, data=(-1, -1, -1, -1))**2)
        self.assertListEqual(a.data, [2, 2, 2, 2])

    def test_exponent(self):
        a = BaseArray(shape=(2,2), dtype=float, data=(1, 2, 4, 5))
        self.assertListEqual(BaseArray.exponent(a, 3).data, [1, 8, 64, 125])
        self.assertListEqual(BaseArray.exponent(3, a).data, [3, 9, 81, 243])

    def test_log(self):
        a = BaseArray(shape=(2, 2), dtype=float, data=(1, 2, 4, 5))
        test_data = BaseArray.log10(a).data
        test_data_2 = [0, 0.301029995, 0.60205999, 0.69897]
        for x in range(len(test_data)):
            self.assertAlmostEqual(test_data[x], test_data_2[x])

import unittest
class CITests():
    
    @classmethod
    def test_print(cls):
        suite = unittest.TestSuite()
        suite.addTest(TestArray('test_to_string'))
        return suite
    
    @classmethod
    def test_sort(cls):
        tests = [
            'test_merge_sort', 
            'test_sort_row_col'
        ]
        suite = unittest.TestSuite()
        suite.addTests(map(TestArray, tests))
        return suite
    
    @classmethod
    def test_search(cls):
        suite = unittest.TestSuite()
        suite.addTest(TestArray('test_search_value'))
        return suite
    
    @classmethod
    def test_math_ops(cls):
        tests = [
            'test_mul_lists',
            'test_addition',
            'test_substraction',
            'test_multiplication',
            'test_division',
            'test_pow',
            'test_exponent',
            'test_log'
        ]
        suite = unittest.TestSuite()
        suite.addTests(map(TestArray, tests))
        return suite
